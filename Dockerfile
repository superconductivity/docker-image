FROM rocker/tidyverse:latest

RUN apt -y update && \
    apt -y upgrade && \
    apt -y install \
        curl \
        gdebi-core \
	libudunits2-dev \
        openssh-client \
	libxt6 \
        && rm -rf /var/lib/apt/lists/*

RUN curl -LO https://quarto.org/download/latest/quarto-linux-amd64.deb && gdebi --non-interactive quarto-linux-amd64.deb

RUN quarto install tinytex
RUN quarto install chromium

CMD ["bash"]
